(function(ns) {

    'use strict';

    var domElems = {
        scrollTopBtn:  null,
        navWrapper:    null,
        errorField:    null,
        mobileIcon:    null,
        mobileSubmenu: null
    };

    /* -- Public scope ---------------------------------------------------------------------------------------------- */

    ns.common = {

        init: function() {

            helper.hideStickyNavigation();

            domElems.scrollTopBtn  = document.getElementById('scroll-top');
            domElems.navWrapper    = document.getElementById('navigation-wrapper');
            domElems.errorField    = document.querySelectorAll('.field-error');
            domElems.mobileIcon    = document.querySelector('.menu-mobile .icon');
            domElems.mobileSubmenu = document.querySelector('.menu-mobile .submenu');

            domElems.scrollTopBtn.addEventListener('click', eventHandlers.onScrollTopBtnClick);
            document.addEventListener('scroll', eventHandlers.onScrollDown);

            if (domElems.errorField) {

                domElems.errorField.forEach(function(element) {
                    element.addEventListener('keypress', eventHandlers.onErrorFieldChange);
                });

                domElems.errorField.forEach(function(element) {
                    element.addEventListener('focus', eventHandlers.onErrorFieldClick);
                });

            }

            if (domElems.mobileIcon) {
                domElems.mobileIcon.addEventListener('click', eventHandlers.onMobileIconClick);
            }

        }

    };

    /* -- Helper (private scope) ------------------------------------------------------------------------------------ */

    var helper = {

        showScrollTopButton: function() {

            domElems.scrollTopBtn.style.opacity = 1;

        },

        hideScrollTopButton: function() {

            domElems.scrollTopBtn.style.opacity = 0;

        },

        showStickyNavigation: function() {

            var clone = domElems.navWrapper.cloneNode(true);
            clone.id = 'navigation-wrapper-clone';

            if (document.getElementById('navigation-wrapper-clone')) {
                return;
            }

            document.body.appendChild(clone);

        },

        hideStickyNavigation: function() {

            if (document.getElementById('navigation-wrapper-clone')) {
                document.body.removeChild(document.getElementById('navigation-wrapper-clone'));
            }

        }

    };

    var eventHandlers = {

        onScrollDown: function() {

            // Show scroll top icon
            if (window.scrollY > 300) {
                helper.showScrollTopButton();
            } else if (window.scrollY <= 200) {
                helper.hideScrollTopButton();
            }

            // Make navigation wrapper sticky
            if (window.scrollY > domElems.navWrapper.offsetTop) {
                helper.showStickyNavigation();
            } else if (window.scrollY < domElems.navWrapper.offsetTop + 10) {
                helper.hideStickyNavigation();
            }

        },

        onScrollTopBtnClick: function() {

            setTimeout(function() {

                if (window.scrollY > 0) {
                    eventHandlers.onScrollTopBtnClick();
                }

                var position = window.scrollY - 100;

                window.scrollTo(null, position);

            }, 10);

        },

        onErrorFieldChange: function(element) {

            element.target.classList.remove('field-error');

            var className    = '.form-error.' + element.target.getAttribute('name');
            var formErrorBox = document.querySelector(className);
            formErrorBox.parentNode.removeChild(formErrorBox);

            element.target.removeEventListener('keypress', eventHandlers.onErrorFieldChange);

        },

        onErrorFieldClick: function(element) {

            element.target.value = '';

            element.target.removeEventListener('focus', eventHandlers.onErrorFieldClick);

        },

        onMobileIconClick: function(element) {

            if (domElems.mobileSubmenu.style.display === 'block') {
                return domElems.mobileSubmenu.style.display = 'none';
            }

            domElems.mobileSubmenu.style.display = 'block';

        }

    };

})(window.namespace('ngeil'));

window.addEventListener('load', window.ngeil.common.init);
