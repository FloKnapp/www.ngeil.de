(function(ns) {

    'use strict';

    var API_URL = '/api/image/';

    var fields = {
        distributorSourceId: null,
        distributorName:     null
    };

    var domElems = {
        additionalButton:  null,
        distributor:       null,
        mediaWrapper:      null,
        additionalWrapper: null,
        loader:            null,
        moreIcon:          null
    };

    /* -- Public scope ---------------------------------------------------------------------------------------------- */

    ns.additional = {

        init: function() {

            domElems.additionalWrapper = document.getElementById('additional-wrapper');
            domElems.mediaWrapper      = document.getElementById('media-wrapper');
            domElems.distributor       = document.getElementById('distributor');
            domElems.additionalButton  = document.getElementById('additional');
            domElems.loader            = document.querySelector('.fa.fa-spin.fa-circle-o-notch');
            domElems.moreIcon          = document.querySelector('.fa.fa-arrow-down');

            if (domElems.distributor) {
                fields.distributorSourceId = domElems.distributor.dataset.sourceId;
                fields.distributorName     = domElems.distributor.innerText.toLowerCase();
            }

            if (domElems.additionalButton) {
                domElems.additionalButton.addEventListener('click', eventHandlers.onAdditionalClick);
            }

        }

    };

    /* -- Private scope --------------------------------------------------------------------------------------------- */

    var privateFunctions = {

        getImages: function() {

            var uri = API_URL + fields.distributorName + '/' + fields.distributorSourceId;

            privateFunctions.doAjax(uri, function(response) {

                var result = JSON.parse(response);

                if (result.code === 200 && result.status === 'ok') {
                    privateFunctions.renderImages(result.data);
                }

            });

            return true;

        },

        renderImages: function(images) {

            var img;

            images.forEach(function(name, index) {

                if (index === 0) {
                    return;
                }

                img = document.createElement('img');

                img.onload = function() {

                    if (this.offsetWidth > 580)
                        this.classList.add('zoom');

                    this.style.maxWidth  = '580px';

                    if (index === images.length - 1) {
                        window.ngeil.image.zoom.init();
                    }

                };

                img.src             = name;
                img.style.display   = 'block';
                img.style.textAlign = 'center';
                img.style.margin    = '10px auto';

                domElems.mediaWrapper.appendChild(img);

            });

            privateFunctions.hideAdditionalButton();

        },

        hideAdditionalButton: function() {

            domElems.additionalWrapper.style.display = 'none';

        },

        doAjax: function(uri, callback) {

            domElems.moreIcon.style.display = 'none';
            domElems.loader.style.display   = 'inline-block';

            var xhr = new XMLHttpRequest();
            xhr.open('GET', uri, true);

            xhr.onreadystatechange = function() {

                if (xhr.readyState === 4 && xhr.status === 200) {
                    callback(xhr.responseText);
                }

                domElems.moreIcon.style.display = 'inline-block';
                domElems.loader.style.display   = 'none';

            };

            xhr.send();

        }

    };

    /* -- Event handlers -------------------------------------------------------------------------------------------- */

    var eventHandlers = {

        onAdditionalClick: function() {

            return privateFunctions.getImages();

        }

    };

})(window.namespace('ngeil.image'));

window.addEventListener('load', window.ngeil.image.additional.init);