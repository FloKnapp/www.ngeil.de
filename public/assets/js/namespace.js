function namespace(fqdn) {

    'use strict';

    var parts  = fqdn.split('.');
    var parent = window;

    for (var i = 0; i < parts.length; i++) {

        if (!parent[parts[i]]) {
            parent[parts[i]] = {};
        }

        parent = parent[parts[i]];

    }

    return parent;

};
