/**
 * Image zoom handler
 *
 * @author Florian Knapp <office@florianknapp.de>
 */
(function(ns, nsCommon) {

    'use strict';

    var fields = {
        activeElement: null
    };

    var domElems = {
        img:      null,
        vid:      null,
        layer:    null,
        content:  null,
        menu:     null
    };

    /* -- Public scope ---------------------------------------------------------------------------------------------- */

    ns.zoom = {

        init: function() {

            domElems.img     = document.querySelectorAll('img');
            domElems.vid     = document.querySelectorAll('video');
            domElems.content = document.querySelector('.content');
            domElems.menu    = document.querySelector('.menu');

            if (domElems.img) {

                domElems.img.forEach(function(element) {

                    if (element.classList.contains('zoom')) {
                        element.addEventListener('click', eventHandlers.onImageClick);
                    }

                });

            }

            if (domElems.vid) {

                domElems.vid.forEach(function(element) {

                    if (element.classList.contains('zoom')) {
                        element.addEventListener('click', eventHandlers.onVideoClick);
                    }

                });

            }

        }

    };

    /* -- Private scope --------------------------------------------------------------------------------------------- */

    var privateFunctions = {

        appendLayer: function() {

            if (document.querySelector('#layer')) {
                return;
            }

            var layer = document.createElement('div');
            layer.id  = 'layer';

            document.body.appendChild(layer);

            layer.addEventListener('click', eventHandlers.onLayerClick);

        },

        removeLayer: function() {

            var layer = document.querySelector('#layer');
            var imageContainer = document.querySelector('#media-container');
            document.body.removeChild(layer);
            document.body.removeChild(imageContainer);

            domElems.menu.classList.remove('blur');
            domElems.content.classList.remove('blur');

            if (window.scrollY > 0) {

                var additionalHeight = 0;

                if (document.getElementById('navigation-wrapper-clone')) {
                    additionalHeight = document.getElementById('navigation-wrapper-clone').offsetHeight;
                }

                var lastPosition = fields.activeElement.offsetTop - additionalHeight;
                window.scrollTo(null, lastPosition);
            }

            if (document.getElementById('navigation-wrapper-clone')) {
                document.getElementById('navigation-wrapper-clone').style.opacity = 0.9;
            }

            window.ngeil.common.init();

        },

        showMedia: function(elem) {

            var width, height = null;

            fields.activeElement = elem;

            domElems.menu.classList.add('blur');
            domElems.content.classList.add('blur');

            var clone = elem.cloneNode(true);
            clone.removeAttribute('style');

            clone.style.maxWidth = '960px';
            clone.classList.add('out');

            var box = document.createElement('div');
            box.id  = 'media-container';

            box.appendChild(clone);
            document.body.appendChild(box);

            if (elem.tagName === 'IMG') {
                width     = clone.offsetWidth;
                height    = clone.offsetHeight;
            } else if (elem.tagName === 'VIDEO') {
                width     = elem.videoWidth;
                height    = elem.videoHeight;
            }

            privateFunctions.setBoxPosition(box, width, height);

            if (document.getElementById('navigation-wrapper-clone')) {
                document.getElementById('navigation-wrapper-clone').style.opacity = 0;
            }

            clone.addEventListener('click', eventHandlers.onLayerClick);

        },

        setBoxPosition: function(box, width, height) {

            var halfWidth        = (width / 2) + 5;

            box.style.width      = width + 'px';
            box.style.marginLeft = '-' + halfWidth + 'px';
            box.style.height     = height + 'px';

            var positionAtTop    = window.scrollY + 20 + 'px';
            var positionAtCenter = window.scrollY + (window.outerHeight / 2) - (box.offsetHeight / 2) - 50 + 'px';

            if (height > window.outerHeight) {
                box.style.top = positionAtTop;
            } else {
                box.style.top = positionAtCenter;
            }

            if (privateFunctions.isVisible(document.querySelector('.menu-mobile'))) {
                box.style.width      = '98%';
                box.style.height     = 'auto';
                box.style.marginLeft = 0;
                box.style.left       = 0;
                box.style.top        = positionAtTop;
            }

        },

        isVisible: function(elem) {

            return !!( elem.offsetWidth || elem.offsetHeight);

        }

    };

    /* -- Event handlers -------------------------------------------------------------------------------------------- */

    var eventHandlers = {

        onImageClick: function() {

            privateFunctions.appendLayer();
            privateFunctions.showMedia(this);

        },

        onVideoClick: function() {

            privateFunctions.appendLayer();
            privateFunctions.showMedia(this);

        },

        onLayerClick: function() {

            privateFunctions.removeLayer();

        }

    };

})(window.namespace('ngeil.image'));

window.addEventListener('load', window.ngeil.image.zoom.init);