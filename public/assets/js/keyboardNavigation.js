/**
 * Keyboard navigation
 *
 * @author Florian Knapp <office@florianknapp.de>
 */

(function(ns) {

    ns.navigation = {

        init: function() {

            document.body.addEventListener('keydown', eventHandlers.onKeyPress);

        }

    };

    var eventHandlers = {

        onKeyPress: function(e) {

            var leftBtn = document.querySelector('.nav.left');
            var additionalBtn = document.getElementById('additional');

            switch (e.keyCode) {

                case 39: // cursor right
                    document.querySelector('.nav.right').click();
                    break;

                case 37: // cursor left
                    if (leftBtn)
                        leftBtn.click();
                    break;

                case 32: // space
                    e.preventDefault();
                    if (additionalBtn)
                        additionalBtn.click();
                    break;

            }

        }

    }


})(window.namespace('js.ngeil.keyboard'));

window.addEventListener('load', window.js.ngeil.keyboard.navigation.init);
