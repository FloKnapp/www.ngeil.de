<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REMOTE_ADDR'] !== '95.91.166.35') {
    die('Nothing to see here');
}

include_once __DIR__ . '/../Core/Autoload.php';

use Core\Dispatcher;
use Core\Http\Request;
use Core\Session\SessionStorage;

define('PROJECT_ROOT',      __DIR__ . '/..');
define('CONFIG_ROOT',       PROJECT_ROOT . '/settings');
define('VIEWS_ROOT',        PROJECT_ROOT . '/views');
define('CORE_ROOT',         PROJECT_ROOT . '/Core');
define('APPLICATION_ROOT',  PROJECT_ROOT . '/Application');
define('MEDIA_PATH',        PROJECT_ROOT . '/../www.ngeil.de/public');

define('HOSTNAME',          'https://' . $_SERVER['HTTP_HOST']);

$sessionStorage = SessionStorage::instance();
$translation    = \Core\Utility\Config::get('translation');

if (isset($_GET['lang']) && in_array($_GET['lang'], array_keys($translation))) {
    $sessionStorage->set('lang', $_GET['lang']);
}

$request = new Request();
$request->createFromHeaders();
$request->setSession($sessionStorage);

return new Dispatcher($request);