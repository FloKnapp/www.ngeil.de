<?php

namespace Core\Mail;

use Core\Exceptions\MailException;

class Mailer
{

    /** @var string */
    protected $recipients = '';

    /** @var string */
    protected $from = '';

    /** @var string */
    protected $subject = '';

    /** @var string */
    protected $body = '';

    /** @var array */
    private $header;

    public function addTo(string $recipient = '')
    {
        $this->recipients[] = $recipient;
    }

    public function setFrom(string $sender = '')
    {
        $this->from = $sender;
    }

    public function setSubject(string $subject = '')
    {
        $this->subject = $subject;
    }

    public function setHtmlBody(string $body = '')
    {
        $this->addHeader('MIME-Version: 1.0');
        $this->addHeader('Content-type: text/html; charset=utf-8');
        $this->setBody($body);
    }

    public function setBody(string $body = '')
    {
        $this->body = $body;
    }

    public function send()
    {
        $recipients = implode(',', $this->recipients);

        $from = empty($this->from) ? "Flozn <message@flozn.de>" : $this->from;
        $this->addHeader('From: ' . $from);

        $result = '';
        foreach ($this->header AS $header) {
            $result .= $header . "\r\n";
        }

        $subject    = $this->subject;
        $body       = $this->body;

        if (!mail($recipients, $subject, $body, $result)) {
            throw new MailException('E-Mail couldn\'t be send');
        }

        return true;
        
    }

    private function addHeader($value)
    {
        $this->header[] = $value;
    }

}