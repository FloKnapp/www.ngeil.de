<?php

namespace Core\Service;

interface ServiceLocatorAwareInterface
{

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator);

}