<?php

namespace Core\Service;

interface FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator);

}