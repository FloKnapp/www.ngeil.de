<?php

namespace Core\Service;

use Core\Utility\Config;

class ServiceLocator implements ServiceLocatorInterface
{

    /** @var ServiceLocator */
    protected static $instance;

    /** @var array */
    protected $services = [];

    /**
     * @return ServiceLocator
     */
    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $name
     * @return ServiceLocatorAwareInterface|null
     * @throws \Core\Exceptions\NotFoundException
     */
    public function get(string $name)
    {
        $config = Config::get('service');

        if ($result = $this->hasService($name)) {

            return $result;

        } else if (isset($config['factory'][$name])) {

            /** @var ServiceLocatorAwareInterface $service */
            $service               = '\\' . $config['factory'][$name];
            $service               = new $service();
            $this->services[$name] = $service->createService($this);

            return $this->services[$name];

        }

        return null;
    }

    /**
     * @param string $name
     * @return \stdClass|false
     */
    protected function hasService(string $name)
    {
        if (isset($this->services[$name])) {
            return $this->services[$name];
        }

        return false;
    }

    /**
     * @return void
     */
    protected function clearServices()
    {
        $this->services = [];
    }

}