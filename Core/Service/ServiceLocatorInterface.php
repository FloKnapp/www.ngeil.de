<?php

namespace Core\Service;

interface ServiceLocatorInterface
{

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name);

}