<?php

class Autoload {

    public static function load($className) {

        $fileName = str_replace("\\", "/", $className) . '.php';
        $filePath = PROJECT_ROOT . '/' . $fileName;
        
        if (file_exists($filePath)) {
            include $filePath;
        } else {
            throw new \Core\Exceptions\NotFoundException('Class ' . $filePath . ' not found...');
        }

        return true;

    }

}

spl_autoload_register(['Autoload', 'load']);