<?php

namespace Core\Security\Crypto;

class Bcrypt
{
    /**
     * @param mixed $value
     * @return string
     */
    public static function hash($value)
    {
        return crypt($value, 'rockzzz');
    }

    /**
     * @param string $hash
     * @param string $userString
     * @return bool
     */
    public static function isEqual(string $hash, string $userString)
    {
        if (password_verify($userString, $hash)) {
            return true;
        }

        return false;
    }
}