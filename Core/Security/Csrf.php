<?php

namespace Core\Security;

use Core\Session\SessionStorage;

class Csrf
{

    /**
     * Generates a token and save it to session
     *
     * @return string
     */
    public static function getToken()
    {
        $token = bin2hex(random_bytes(32));
        self::saveToSession($token);
        return $token;
    }

    private static function saveToSession($token)
    {
        SessionStorage::instance()->setFlashbag('csrf', $token);
    }

}