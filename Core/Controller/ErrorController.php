<?php

namespace Core\Controller;

class ErrorController extends AbstractController
{

    /** @var string */
    public static $requiredPermission = '';

    /**
     * @return string
     */
    public function notFound()
    {
        $this->addDefaultAssets();

        header('HTTP/2 404 Not found');

        return $this->render('/error/not-found.phtml');
    }

    /**
     * @return string
     */
    public function notPermittedAction()
    {
        $this->addDefaultAssets();

        header('HTTP/2 401 Unauthorized');

        return $this->render('/error/not_permitted.phtml');
    }

    /**
     * Helper for adding default assets which are used in different methods
     */
    private function addDefaultAssets()
    {
        $this->addAsset('https://fonts.googleapis.com/css?family=Monda|Roboto+Condensed');
        $this->addAsset('/css/main.css');
        $this->addAsset('/css/font-awesome/font-awesome.min.css');
        $this->addAsset('/js/namespace.js');
        $this->addAsset('/js/imageZoom.js');
        $this->addAsset('/js/imageAdditional.js');
        $this->addAsset('/js/common.js');
    }

}