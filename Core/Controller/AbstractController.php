<?php

namespace Core\Controller;

use Application\Service\UserService;
use Core\Exceptions\NotFoundException;
use Core\Http\Request;
use Core\View\ViewController;
use Core\Service\ServiceLocator;

abstract class AbstractController
{

    public static $requiredPermission = '';

    /** @var array */
    protected $viewArray = [];

    /** @var Request */
    protected $request;

    /**
     * AbstractController constructor.
     * @param Request $request
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * @return ViewController
     */
    public function getView()
    {
        $calledClass = get_called_class();

        if (in_array($calledClass, array_keys($this->viewArray))) {
            return $this->viewArray[$calledClass];
        }

        $viewController = new ViewController();
        $this->viewArray[$calledClass] = $viewController;

        return $viewController;
    }

    /**
     * @param string $file
     * @throws NotFoundException
     */
    public function addAsset(string $file)
    {
        if (strpos($file, 'js') !== false) {
            $method = 'setScripts';
        } else if (strpos($file, 'css') !== false) {
            $method = 'setStylesheets';
        } else {
            throw new NotFoundException('No compatible asset format given ' . $file);
        }

        $this->getView()->$method($file);
    }

    /**
     * @param string $template
     * @param array $variables
     * @return string
     */
    public function render(string $template, array $variables = []) {

            $view = $this->getView();
            $view->setTemplate($template);
            $view->setVariables($variables);
            return $view->render();

    }

    /**
     * @return ServiceLocator
     */
    protected function getServiceLocator()
    {
        return ServiceLocator::instance();
    }

}