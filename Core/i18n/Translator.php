<?php

namespace Core\i18n;

use Core\Session\SessionStorage;
use Core\Utility\Config;

class Translator
{

    protected $config;

    protected $language;

    public function __construct($language = 'ger_DE')
    {

        $this->language = $language;

        $sessionStorage = SessionStorage::instance();

        if ($sessionStorage->get('lang')) {
            $this->language = $sessionStorage->get('lang');
        }

        $this->config   = Config::get('translation');
    }

    public function translate($key, $value = null)
    {
        if (isset($this->config[$this->language][$key])) {

            if ($value !== null) {
                return sprintf($this->config[$this->language][$key], $value);
            }

            return $this->config[$this->language][$key];
        }
        return $key;
    }

}