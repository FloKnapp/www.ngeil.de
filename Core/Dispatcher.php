<?php

namespace Core;

use Core\Controller\ErrorController;
use Core\Exceptions\NotFoundException;
use Core\Form\AbstractFormHandler;
use Core\Form\BaseFormHandler;
use Core\Http\Request;
use Core\Http\Response;
use Core\Utility\Config;

class Dispatcher
{
    
    /** @var Request */
    protected $request = null;

    public function __construct(Request $request) {

        $this->request = $request;
        
        try {

            // Try to assign uri to an existing route
            $assignment = $this->assign($request->getUri());

            // Prefix controller with fqdn identifier
            $controller = '\\' . $assignment['ctrl'];

            // By default there are no parameters
            $parameter  = null;

            if (isset($assignment['action']) && !isset($assignment['parameter'])) {

                // Now its a bit tricky - we check on an given action in settings
                $action = $assignment['action'] . "Action";

            } else if (isset($assignment['action']) && isset($assignment['parameter'])) {

                $action = $assignment['action'] . "Action";
                $parameter = str_replace($assignment['route'], '', $assignment['parameter']);

                // If we got no action, take the uri part after the ctrl part for action
                $parts = explode('/', $parameter);

                while (empty($parts[0]) && count($parts) > 0) {
                    array_shift($parts);
                }

                $parameter = $parts;

            } else if (isset($assignment['parameter'])) {

                // If we got no action, take the uri part after the ctrl part for action
                $parts = explode('/', $assignment['parameter']);

                while (empty($parts[0]) && count($parts) > 0) {
                    array_shift($parts);
                }

                $action = $parts[0] . "Action";
                array_shift($parts);
                $parameter = $parts;

            } else {
                throw new NotFoundException('Params method missing');
            }

            $ctrl = new $controller($request);

            if (!method_exists($ctrl, $action)) {
                throw new NotFoundException('Method ' . $action . ' in Controller ' . $controller . ' not found');
            }

            // Now trying to run our combination of ctrl and action
            return $ctrl->$action($parameter);

        } catch (\Exception $e) {

            // A nice error site is shown
            return ( new ErrorController($request) )->notFound();

        }

    }

    private function assign(string $uri) {

        // Detect form validation requests
        $this->detectFormHandlerRequest($uri);

        // Get routing configuration
        $config = Config::get('routes');

        // Iterate over each routing settings
        foreach ($config AS $routeName => $routeParams) {

            if ((!isset($routeParams['static']) || $routeParams['static'] === true) && $routeParams['route'] === $uri) {

                // If there is a full match on the request uri in the settings return the settings block
                return $routeParams;

            }

        }

        foreach ($config AS $routeName => $routeParams) {

            if ((isset($routeParams['static']) && $routeParams['static'] === false)
                && strpos($uri, $routeParams['route'])  === 0
                && strlen($uri) > strlen($routeParams['route'])
            ) {

                // If we got the 'hasParams' identifier cut the ctrl name out of uri and return it
                return $routeParams + ['parameter' => $uri];

            }

        }

        // This should only happen if:
        // - no routes are configured
        // - a typo is hiding in configuration
        // - false params are in configuration
        throw new NotFoundException('Assignment cannot be done for route ' . $uri);

    }

    private function detectFormHandlerRequest($uri) {

        if (strpos($uri, '/Handler') !== 0) {
            return false;
        }

        $validatorPath = PROJECT_ROOT . '/Application/Form' . $uri . 'Handler.php';

        if (file_exists($validatorPath)) {

            $path       = str_replace('/', '\\', $uri);
            $className  = '\\Application\\Form' . $path . 'Handler';

            /** @var AbstractFormHandler $class */
            $class = new $className($this->request);

            die($class->run());

        }

        return false;

    }

}