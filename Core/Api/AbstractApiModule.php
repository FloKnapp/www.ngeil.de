<?php

namespace Core\Api;

use Core\Dal\Database;
use Core\Dal\Driver\MySql;
use Core\Dal\Driver\Redis;

abstract class AbstractApiModule
{
    /**
     * @return MySql
     */
    public function getDb()
    {
        return (new Database('mysql'))->getDriver();
    }

    /**
     * @return Redis
     */
    public function getRedis()
    {
        return (new Database('redis'))->getDriver();
    }

    /**
     * @param mixed $data
     */
    public function success($data)
    {
        header('HTTP/2.0 200 Ok');
        header('Content-Type: application/json; charset=utf-8');
        die(json_encode(
            [
                'code'   => 200,
                'status' => 'ok',
                'data'   => $data
            ]
        ));
    }

    /**
     * @param mixed $data
     */
    public function error($data)
    {
        header('HTTP/2.0 400 Bad Request');
        header('Content-Type: application/json; charset=utf-8');
        die(json_encode(
            [
                'code'   => 400,
                'status' => 'error',
                'data'   => $data
            ]
        ));
    }
}