<?php

namespace Core\Form;

abstract class AbstractValidator
{

    /**
     * @return array
     */
    public abstract function validationOptions();

}