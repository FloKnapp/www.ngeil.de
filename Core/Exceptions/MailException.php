<?php

namespace Core\Exceptions;

class MailException extends BaseException
{
    public function __construct($message)
    {
        parent::__construct($message, 430);
    }
}