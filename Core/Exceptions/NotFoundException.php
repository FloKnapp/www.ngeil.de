<?php

namespace Core\Exceptions;

class NotFoundException extends BaseException
{
    public function __construct($message)
    {
        parent::__construct($message, 404);
    }
}