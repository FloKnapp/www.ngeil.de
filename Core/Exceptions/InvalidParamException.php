<?php

namespace Core\Exceptions;

class InvalidParamException extends BaseException
{
    public function __construct($message)
    {
        parent::__construct($message, 410);
    }
}