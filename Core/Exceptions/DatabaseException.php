<?php

namespace Core\Exceptions;

class DatabaseException extends BaseException
{
    public function __construct($message)
    {
        parent::__construct($message, 420);
    }
}