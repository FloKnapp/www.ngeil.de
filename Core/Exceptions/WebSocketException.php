<?php
/**
 * www.flozn.de SocketException.php
 * ---
 *
 * @author Florian Knapp <office@florianknapp.de>
 */

namespace Core\Exceptions;


class WebSocketException extends \Exception
{
    // all inherited
}