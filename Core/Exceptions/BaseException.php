<?php

namespace Core\Exceptions;

class BaseException extends \Exception
{

    public function __construct($message, $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);

        switch ($code) {

            case 404:
                $type = 'error';
                break;

            case 410:
                $type = 'error';
                break;

            case 420:
                $type = 'error';
                break;

            case 430:
                $type = 'error';
                break;

            default:
                $type = 'error';
                break;

        }
        
        $message = date('d.m.Y H:i:s', time()) . ' | ' . get_called_class() . ' | ' . $message . "\n";

        if (file_exists('logs/' . $type . '.log')) {
            file_put_contents('logs/' . $type . '.log', $message, FILE_APPEND);
        }
    }

}