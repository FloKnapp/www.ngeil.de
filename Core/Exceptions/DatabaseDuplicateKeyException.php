<?php
/**
 * Created by PhpStorm.
 * User: Flo
 * Date: 07.11.2016
 * Time: 10:55
 */

namespace Core\Exceptions;


class DatabaseDuplicateKeyException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message, 421);
    }
}