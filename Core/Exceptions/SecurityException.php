<?php

namespace Core\Exceptions;

class SecurityException extends BaseException
{
    public function __construct($message) {
        parent::__construct($message, 401);
    }
}