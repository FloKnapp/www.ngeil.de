<?php

namespace Core\Log;

use Core\Exceptions\InvalidParamException;

class Logger
{

    protected static $instance;

    protected static $level = [
        'debug',
        'warning',
        'error'
    ];

    protected $messages;

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function write($message = '', string $level = 'error') {

        if (!in_array($level, self::$level)) {
            throw new InvalidParamException('No valid level given');
        }

        if (file_exists(PROJECT_ROOT . '/logs/' . $level . '.log')) {
            file_put_contents(PROJECT_ROOT . '/logs/' . $level . '.log', $message, FILE_APPEND);
        }

        $this->messages[] = $message;

    }

    public function getMessages()
    {
        var_dump($this->messages);

        return true;
    }

}