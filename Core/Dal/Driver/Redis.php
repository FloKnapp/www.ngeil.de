<?php

namespace Core\Dal\Driver;

use Core\Exceptions\DatabaseException;

class Redis
{

    /** @var resource */
    protected $con = null;

    protected $errno;

    protected $errstr;

    public function __construct($config)
    {
        $this->con = fsockopen($config['host'], $config['port'], $this->errno, $this->errstr, 3);

        if (!$this->con) {
            throw new DatabaseException('Can not connect to redis-cli on ' . $config['host'] . ':' . $config['port']);
        }
    }

    public function set($key, $value)
    {
        $command = 'set ' . $key . ' ' . $value;

        $int = fwrite($this->con, $command, strlen($command));

        return $int;
    }

    public function get($key)
    {
        $command = 'get ' . $key;

        $int = fwrite($this->con, $command, strlen($command));

        return $int;
    }

    public function __destruct()
    {
        fclose($this->con);
    }

}