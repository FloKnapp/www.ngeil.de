<?php

namespace Core\Dal\Driver;

use Core\Exceptions\DatabaseException;

class MySql
{

    protected $db;

    public function __construct($config) {

        $this->db = mysqli_connect($config['host'], $config['user'], $config['pwd']);
        mysqli_select_db($this->db, $config['db']);

        if (!$this->db) {
            throw new DatabaseException('Can not connect to database');
        }

    }

    public function query(string $query)
    {
        return $this->db->query($query);
    }

    public function escape($string)
    {
        return mysqli_real_escape_string($this->db, $string);
    }

    public function getResource()
    {
        return $this->db;
    }

    public function __destruct()
    {
        $this->db->close();
    }



}