<?php

namespace Core\Dal;

use Core\Utility\Config;

class Database
{

    /** @var array */
    protected $resource;

    protected $driver;

    public function __construct(string $driver)
    {
        $config       = Config::get('database');
        $driverNs     = __NAMESPACE__ . '\Driver\\';
        $driverClass  = $config[$driver]['driver'];
        $driverConf   = $config[$driver];

        $driver       = $driverNs . $driverClass;
        
        $this->driver = new $driver($driverConf);
    }

    public function getDriver()
    {
        return $this->driver;
    }

}