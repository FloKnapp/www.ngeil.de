<?php

namespace Core\Model\Entity;

/**
 * Class AbstractEntity
 * @package Core\Model\Entity
 *
 * @method getId()
 */
abstract class AbstractEntity implements \JsonSerializable
{

    public static $tableName = '';

    /**
     * @param array $data
     * @return $this
     */
    public function fill($data = [])
    {
        foreach ($data AS $key => $value) {

            if (strpos($key, '__') !== false) {
                $tableName = explode('__', $key)[0];
                $key = str_replace($tableName . '__', '', $key);
            }

            $key = str_replace('_', '', $key);
            $setMethod = "set" . ucfirst($key);
            $getMethod = "get" . ucfirst($key);

            $isDirty = $this->$getMethod() !== $value;

            if (method_exists($this, $setMethod) && $isDirty) {
                $this->$setMethod($value);
            }

        }

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            static::$tableName => [
                get_object_vars($this),
            ]
        ];
    }

}