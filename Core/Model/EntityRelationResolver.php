<?php

namespace Core\Model;

use Core\Dal\Database;
use Core\Dal\Driver\MySql;
use Core\Model\Entity\AbstractEntity;

class EntityRelationResolver
{

    /** @var AbstractEntity */
    protected $entity;

    /** @var MySql */
    protected $db;

    /**
     * EntityRelationResolver constructor.
     * @param AbstractEntity $entity
     * @param MySql $db
     */
    public function __construct(AbstractEntity $entity, $db = null)
    {
        $this->entity = $entity;

        if ($db === null) {
            $db = new Database('mysql');
        }

        $this->db = $db->getDriver();

        return $this->getRelations();
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getRelations()
    {
        $entity     = $this->entity;
        $reflection = new \ReflectionClass($entity);
        $namespace  = $reflection->getNamespaceName();
        $joins      = $reflection->getConstants();
        $fields     = $reflection->getProperties(\ReflectionProperty::IS_PROTECTED);

        $prop                 = null;
        $relation             = null;
        $relationMap          = null; // This holds the key from base entity property to the name of the related entity name
        $relationTableColumns = null; // This we need only in first iteration

        foreach ($joins AS $field => $table) {

            $relationTableColumns = [];

            if (strpos($field, 'join_') !== false) {

                $name               = str_replace('join_', '', $field);
                $columns            = $this->db->query('SHOW COLUMNS FROM ' . $table)->fetch_all();
                $relationMap[$name] = $table;

                foreach ($columns AS $column) {
                    $relationTableColumns[] = $column[0];
                }

                $relation[$name] = [$table => $relationTableColumns];

            }
            
        }

        // Get properties from base class
        foreach ($fields AS $property) {
            $prop[] = $property->getName();
        }

        return [
            'namespace'     => $namespace,
            'currentClass'  => get_class($entity),
            'entityTable'   => $entity::$tableName,
            'entityColumns' => $prop,
            'relationMap'   => $relationMap,
            'relationData'  => $relation,
        ];
    }

}