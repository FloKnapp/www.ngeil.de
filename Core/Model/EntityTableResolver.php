<?php

namespace Core\Model;

use Core\Dal\Driver\MySql;
use Core\Model\Entity\AbstractEntity;
use Core\Dal\Database;

class EntityTableResolver
{

    /** @var AbstractEntity */
    protected $entity;
    
    /** @var MySql */
    protected $db;

    public function __construct(AbstractEntity $entity, Database $db = null)
    {
        $this->entity = $entity;
        
        if ($db === null) {
            $this->db = (new Database('mysql'))->getDriver();
        }
    }

    public function getEntityName()
    {
        $entity = $this->entity;
        return $entity::$tableName;
    }

    public function getEntityProperties()
    {
        $result = [];

        $reflection = new \ReflectionClass($this->entity);
        $properties = $reflection->getProperties(\ReflectionProperty::IS_PROTECTED);

        foreach ($properties AS $property) {
            $result[] = $property->name;
        }

        return $result;

    }

    public function getEntityTableColumns()
    {
        $entityTable = $this->getEntityName();

        $columns = [];
        $tables  = $this->db->query('SHOW COLUMNS FROM ' . $entityTable)->fetch_all();

        foreach ($tables AS $column) {
            $columns[] = $column[0];
        }

        return [
            'tableName' => $entityTable,
            'columns' => $columns,
        ];
    }

    public function buildEntityAffectedFieldsAndValues($entity = null)
    {
        if ($entity !== null) {
            $this->entity = $entity;
        }

        $result     = '';
        $properties = $this->getEntityProperties();

        foreach ($properties AS $prop) {

            $methodName = 'get' . str_replace('_', '', $prop);

            if (!method_exists($this->entity, $methodName)) {
                continue;
            }

            $value = $this->entity->$methodName();

            if (empty($value)) {
                continue;
            }

            if (is_object($value)) {
                /** @var AbstractEntity $value */
                $result .= $prop . ' = \'' . $value->getId() . '\', ';
            } else {
                $result .= $prop . ' = \'' . $this->db->escape($value) . '\', ';
            }

        }

        return substr($result, 0, strlen($result) - 2);
    }

    public function getStrategy()
    {

        $entityTable = $this->getEntityName();
        $result      = $this->db->query('SELECT source_id FROM ' . $entityTable . ' WHERE source_id = \'' . $this->entity->getSourceId() . '\'');

        $strategy    = 'insert';

        if ($result && $result->num_rows > 0) {
            $strategy = 'update';
        }

        return $strategy;
    }

}