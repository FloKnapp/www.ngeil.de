<?php

namespace Core\Model;

class EntityQueryBuilder
{

    /** @var EntityRelationResolver */
    protected $resolver;

    /** @var string */
    protected $query = '';

    protected $where = '';

    protected $orderBy = '';

    protected $limit = '';

    public function __construct(EntityRelationResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function where(array $where = [])
    {

        if (!empty($where)) {

            $this->where .= ' WHERE ';

            foreach ($where AS $key => $value) {
                $this->where .= $key . ' = \'' . $value . '\'';

                if (count($where) > 1) {
                    $this->where .= ' AND ';
                }

            }

        }

        return $this;
    }

    public function orderBy(array $orderBy = [])
    {

        if (!empty($orderBy) && isset(array_values($orderBy)[0])) {

            $this->orderBy .= ' ORDER BY ';
            $this->orderBy .= key($orderBy) . ' ' . array_values($orderBy)[0];

        }

        return $this;
    }

    public function limit(int $limit)
    {
        if ($limit !== 0) {
            $this->limit .= ' LIMIT ' . $limit;
        }
        
        return $this;
    }

    public function selectCount($field) {
        $entity = $this->resolver->getEntity();
        return 'SELECT COUNT(' . $field . ') FROM ' . $entity::$tableName;
    }

    public function getQuery()
    {
        return $this->build();
    }

    private function build()
    {

        $entityData = $this->resolver->getRelations();

        $select = [];

        // Build a select query
        $this->query = 'SELECT ';

        // Iterate over each properties from base class
        foreach ($entityData['entityColumns'] AS $mainTableProp) {
            $select[] = substr($entityData['entityTable'], 0, 6) . '.' . $mainTableProp . ' AS ' . $entityData['entityTable'] . '__' . $mainTableProp;
        }

        // If we got relations, iterate over them and build an correct field select string
        if (count($entityData['relationData'])) {

            foreach ($entityData['relationData'] AS $table) {

                foreach ($table AS $name => $columns) {

                    foreach ($columns AS $column) {

                        $select[] = substr($name, 0, 6) . '.' . $column . ' AS ' . $name . '__' . $column;

                    }

                }

            }

        }

        // Now glue them together by a comma
        $this->query .= implode(', ', $select);

        // Select FROM for base class/entity/table
        $this->query .= ' FROM ' . $entityData['entityTable'] . ' AS ' . substr($entityData['entityTable'], 0, 6);

        // Again: if we got relations, build a join query
        if (count($entityData['relationData'])) {

            foreach ($entityData['relationData'] AS $field => $table) {

                $this->query .= ' LEFT JOIN ' . key($table) . ' AS ' . substr(key($table), 0, 6);
                $this->query .= ' ON ' . substr($entityData['entityTable'], 0, 6) . '.' . $field . ' = ' . substr(key($table), 0, 6) . '.id';

            }

        }

        $this->query .= $this->where;

        $this->query .= $this->orderBy;

        $this->query .= $this->limit;

        return $this->query;

    }

}