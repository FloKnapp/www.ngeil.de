<?php

namespace Core\Model;

use Core\Dal\Database;
use Core\Dal\Driver\MySql;
use Core\Exceptions\DatabaseDuplicateKeyException;
use Core\Exceptions\NotFoundException;
use Core\Service\ServiceLocator;
use Core\Model\Entity\AbstractEntity;
use Core\Exceptions\DatabaseException;
use Core\Service\ServiceLocatorInterface;

/**
 * Class BaseRepository
 * @package Core\Model
 */
abstract class AbstractRepository
{

    protected $excludedFieldsOnSave = [];

    protected $table    = null;
    protected $id       = null;
    protected $class    = null;
    protected $strategy = 'insert';

    /** @var string */
    protected $entityName;

    /** @var AbstractEntity */
    protected $entity;

    /**
     * AbstractRepository constructor.
     * @param AbstractEntity $entity
     */
    public function __construct($entity)
    {
        if ($entity instanceof AbstractEntity) {
            $this->entity = $entity;
            $this->entityName = get_class($entity);
        } else {
            $this->entity = new $entity();
            $this->entityName = $entity;
        }
    }

    /**
     * @param int $id
     * @return AbstractEntity
     * @throws NotFoundException
     */
    protected function getOneById(int $id)
    {
        $entity = $this->getEntity();
        $where = [substr($entity::$tableName, 0, 6) . '.id' => $id];
        $entity = $this->getOne($this->getEntity(), $where);

        if (empty($entity)) {
            throw new NotFoundException('No user found with this query');
        }

        return $entity;
    }

    /**
     * @param string $value
     * @param string $field
     * @return AbstractEntity
     */
    protected function getOneByName(string $value, string $field = '')
    {
        $where = [$field => $value];
        $entity = $this->getOne($this->getEntity(), $where);

        return $entity;
    }

    /**
     * @param $entity
     * @param array $where
     * @return AbstractEntity|array
     * @throws NotFoundException
     */
    protected function getOne($entity, array $where = [])
    {
        $entityRelationResolver  = new EntityRelationResolver($entity);
        $entityQueryBuilder      = new EntityQueryBuilder($entityRelationResolver);

        $query = $entityQueryBuilder->where($where)->getQuery();
        $data  = $this->getDb()->query($query)->fetch_assoc();

        if (!count($data)) {
            return [];
        }

        /** @var AbstractEntity $entity */
        $entity = (new EntityMapper($entityRelationResolver, $data))->mapData();

        return $entity;
    }

    /**
     * @param integer $limit
     * @param array   $sortOrder
     * @param array   $where
     * @return array
     * @throws NotFoundException
     */
    protected function getMany(int $limit = 5, array $sortOrder = [], array $where = [])
    {
        $entities = [];

        $entityRelationResolver  = new EntityRelationResolver($this->getEntity());
        $enQueryBuilder = new EntityQueryBuilder($entityRelationResolver);

        $query = $enQueryBuilder->where($where)->orderBy($sortOrder)->limit($limit)->getQuery();
        $data  = $this->getDb()->query($query)->fetch_all(MYSQLI_ASSOC);

        if (!count($data)) {
            return [];
        }

        foreach ($data AS $result) {
            $entities[] = (new EntityMapper($entityRelationResolver, $result))->mapData();
        }

        return $entities;
    }

    /**
     * @return boolean
     * @throws DatabaseException
     * @throws DatabaseDuplicateKeyException
     */
    public function save()
    {
        /** @var MySql $db */
        $db = $this->getDb();

        /** @var AbstractEntity $entity */
        $entity = $this->getEntity();

        /** @var string $entityTable */
        $entityTable = $entity::$tableName;

        $this->onBeforeSave($this->getEntity());

        $entityTableResolver = new EntityTableResolver($this->getEntity());

        $query = new EntityQueryBuilder(new EntityRelationResolver($this->getEntity()));
        $query->where(['source_id' => $this->getEntity()->getSourceId()]);

        $data = $this->getDb()->query($query->getQuery())->fetch_assoc();

        if (!empty($data)) {
            $this->strategy = 'update';
            $this->entity   = $this->entity->fill($data);
        }

        switch ($this->strategy) {

            case 'update':

                $this->onBeforeUpdate($entity);
                $fields  = $entityTableResolver->buildEntityAffectedFieldsAndValues($this->getEntity());
                $execute = $db->query('UPDATE ' . $entityTable . ' SET ' . $fields . ' WHERE id = \'' . $this->entity->getId() . '\'');

                break;

            case 'insert':

                $this->onBeforeInsert($entity);
                $fields  = $entityTableResolver->buildEntityAffectedFieldsAndValues($this->getEntity());
                $execute = $db->query('INSERT INTO ' . $entityTable . ' SET ' . $fields);
                break;

            default:
                $execute = null;
                break;

        }

        if ($execute === true) {
            $this->onAfterSave($this->getEntity());
            return mysqli_insert_id($db->getResource());
        }

        $this->onSaveError($this->getEntity());

        if (mysqli_errno($db->getResource()) === 1062) {
            var_dump(mysqli_error($db->getResource()));
            throw new DatabaseDuplicateKeyException('Duplicate key detected');
        }

        throw new DatabaseException(
            'Can not execute ' . $this->strategy . ' for table ' . $entityTable
        );
    }

    /**
     * @return AbstractEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    public function getStrategy()
    {
        return $this->strategy;
    }

    /**
     * @return MySql
     */
    protected function getDb()
    {
        return (new Database('mysql'))->getDriver();
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return ServiceLocator::instance();
    }
    
    /** @param AbstractEntity $obj */
    protected function onBeforeSave($obj) {}

    /** @param AbstractEntity $obj */
    protected function onAfterSave($obj) {}

    /** @param AbstractEntity $obj */
    protected function onSaveError($obj) {}

    /** @param AbstractEntity $obj */
    protected function onBeforeInsert($obj) {}

    /** @param AbstractEntity $obj */
    protected function onBeforeUpdate($obj) {}

    /** @param AbstractEntity $obj */
    protected function onBeforeLoad($obj) {}

    /** @param AbstractEntity $obj */
    protected function onAfterLoad($obj) {}
}