<?php

namespace Core\Model;

use Core\Model\Entity\AbstractEntity;
use Core\Log\Logger;

class EntityMapper
{

    /** @var EntityRelationResolver */
    protected $resolver;

    /** @var AbstractEntity */
    protected $entity;

    /** @var array */
    protected $data;

    public function __construct(EntityRelationResolver $resolver, array $data = [])
    {
        $this->resolver = $resolver;
        $this->data     = $data;
    }

    public function mapData()
    {
        $result       = [];
        $entityData   = $this->resolver->getRelations();
        $this->entity = new $entityData['currentClass']();

        // Now separate the results to each corresponding entity name
        foreach ($this->data AS $field => $value) {

            $parts = explode('__', $field);

            if ($entityData['relationMap'] === null) {
                $result[$parts[1]] = $value;
                continue;
            }

            // Set the dataset only if no related key is found (because its getting set twice)
            if (!in_array($parts[1], array_keys($entityData['relationMap']))) {
                $result[$parts[0]][$parts[1]] = $value;
            }

        }

        if ($entityData['relationMap'] === null) {
            return $this->entity->fill($result);
        }

        try {

            // Now iterate over each related entity to fill its with its own values
            foreach ($entityData['relationMap'] AS $entityProperty => $relationKey) {

                $relClass = $entityData['namespace'] . '\\' . ucfirst($relationKey) . 'Entity';

                /** @var AbstractEntity $class */
                $class = new $relClass();
                $class->fill($result[$relationKey]);

                // And here goes the magic - the related key in base entity gets set with the related entity
                $method = 'set' . ucfirst($entityProperty);

                $this->entity->$method($class);
            }

        } catch (\Exception $e) {
            Logger::instance()->write($e->getMessage());
        }

        // Fill the whole shit up
        $entity = $this->entity->fill($result[$entityData['entityTable']]);

        return $entity;
    }

}