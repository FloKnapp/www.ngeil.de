<?php

namespace Core\Http;

class Request extends AbstractHttp
{

    public function createFromHeaders()
    {
        $uri = $_SERVER['REQUEST_URI'];

        if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
            $uri = explode('?', $_SERVER['REQUEST_URI'])[0];
        }

        $this->setUri($uri);
        $this->setMethod($_SERVER['REQUEST_METHOD']);
    }

    public function isPost()
    {
        return count($_POST) ? true : false;
    }

    public function isGet()
    {
        return count($_GET) ? true : false;
    }

    public function getPostData()
    {
        return $_POST;
    }

}