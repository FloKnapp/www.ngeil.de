<?php

namespace Core\Http;

use Core\Session\SessionStorage;

abstract class AbstractHttp
{

    /** @var string */
    protected $method;

    /** @var string */
    protected $uri;

    protected $session;

    /**
     * @param string $method
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    public function setSession(SessionStorage $session)
    {
        $this->session = $session;
    }

    public function getSession()
    {
        return $this->session;
    }

}