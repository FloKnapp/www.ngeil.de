<?php

namespace Core\View;

interface ViewHelperInterface
{

    function getView();
    
    function getServiceLocator();

}