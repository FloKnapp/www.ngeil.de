<?php

namespace Core\View;

use Core\Exceptions\NotFoundException;
use Core\Utility\Config;

class ViewController
{

    /** @var array */
    private $variable           = [];

    /** @var string */
    private $template           = "";

    /** @var ViewController */
    private $extendedTemplate   = null;

    /** @var array */
    private $assetStylesheets   = [];

    /** @var array */
    private $assetScripts       = [];

    /**
     * @param string $template
     * @throws NotFoundException
     * @return $this
     */
    public function setTemplate( string $template = "" )
    {
        $this->template = VIEWS_ROOT . '/' . $template;

        if (!file_exists($this->template)) {
            throw new NotFoundException('Template "' . $this->template . '" not found');
        }

        return $this;
    }

    /**
     * @param string $file
     * @return $this
     */
    public function setScripts(string $file)
    {
        $this->assetScripts[] = $file;
        return $this;
    }

    /**
     * @param string $file
     * @return $this
     */
    public function setStylesheets(string $file)
    {
        $this->assetStylesheets[] = $file;
        return $this;
    }
    
    public function setAssetsJs(array $assets)
    {
        $this->setVariable('assetsJs', $assets);
        return $this;
    }

    public function setAssetsCss(array $assets)
    {
        $this->setVariable('assetsCss', $assets);
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return (string) $this->template;
    }

    /**
     * @param string $key
     * @param string|array $value
     */
    public function setVariable(string $key = "", $value = null)
    {
        $this->variable[$key] = $value;
    }

    /**
     * @param $key
     * @return boolean|string|array
     */
    public function getVariable(string $key)
    {
        if(isset($this->variable[$key])) {
            return $this->variable[$key];
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasVariable(string $key)
    {
        if(isset($this->variable[$key])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $variables
     * @return ViewController $this
     */
    public function setVariables(array $variables = [])
    {
        foreach($variables AS $key=>$value) {
            $this->setVariable($key, $value);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variable;
    }

    /**
     * @param ViewController $view
     */
    public function setExtendedTemplate(ViewController $view)
    {
        $this->extendedTemplate = $view;
    }

    /**
     * @return ViewController
     */
    public function getExtendedTemplate()
    {
        return $this->extendedTemplate;
    }

    /**
     * @param $output
     * @return string
     */
    private function cleanOutput($output)
    {
        return str_replace(array("\t", "\r", "  "), "", trim($output));
    }

    /**
     * @return string
     */
    public function render()
    {
        /** @var ViewHelper $viewFunctions Expose View Functions to its Template*/
        $v = new ViewHelper( $this );
        extract( [$v] );
        extract( $this->variable );

        ob_start();

        include $this->getTemplate();

        $content = ob_get_contents();

        ob_end_clean();

        if( $this->getExtendedTemplate() instanceof ViewController ) {
            echo $this->cleanOutput($this->getExtendedTemplate()->setVariables($this->getVariables())->setAssetsJs($this->assetScripts)->setAssetsCss($this->assetStylesheets)->render());
        } else {
            echo $this->cleanOutput($content);
        }
    }

    /**
     * Magic method for providing a view helper
     *
     * @param $name
     * @param $arguments
     * @return null
     * @throws NotFoundException
     */
    public function __call($name, $arguments)
    {
        $config = Config::get('service');

        if (isset($config['viewHelper'][$name])) {

            $className = $config['viewHelper'][$name];

            if (method_exists(new $className(), '__construct')) {
                return call_user_func_array([new $className(), '__construct'], $arguments);
            }

            return new $config['viewHelper'][$name]($arguments);
        }

        return null;
    }

    /**
     * Destructor
     */
    public function __destruct() 
    {
        unset( $this->variable );
        unset( $this->template );
    }

}