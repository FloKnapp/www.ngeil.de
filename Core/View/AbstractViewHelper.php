<?php

namespace Core\View;

use Core\Service\ServiceLocator;

/**
 * Class AbstractViewHelper
 * @package Core\View
 *
 * @method __invoke()
 */
abstract class AbstractViewHelper
{

    protected function renderView(string $template = '', array$variables = [])
    {
        $templatePath = VIEWS_ROOT . '/_helper';
        return (new ViewController())->setTemplate($templatePath . $template)->setVariables($variables)->render();
    }

    protected function getServiceLocator()
    {
        return ServiceLocator::instance();
    }

    public function __toString()
    {
        return (string)$this->__invoke();
    }

}