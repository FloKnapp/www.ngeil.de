<?php

namespace Core\Utility;

use Core\Exceptions\NotFoundException;

class Config
{

    public static function get(string $name) {

        $filePath = CONFIG_ROOT . '/' . $name . '.conf.php';

        if (file_exists($filePath)) {

            $content = require $filePath;

            if (!isset($content[$name])) {
                throw new NotFoundException('Configuration key ' . $name . ' not found');
            }

            return $content[$name];

        }

        throw new NotFoundException('Configuration ' . $filePath . ' not found');

    }

}