<?php

namespace Api;

use Core\Exceptions\NotFoundException;
use Core\Api\AbstractApiModule;

class ApiController
{

    /**
     * @param array $parameter
     * @return AbstractApiModule
     * @throws NotFoundException
     */
    public function apiAction(array $parameter = [])
    {
        // Check for api module
        if (!isset($parameter[0])) {
            throw new NotFoundException('No api module was given');
        }

        // Set module namespace
        $module = $parameter[0];
        $module = '\\Api\\Module\\' . ucfirst($module);

        // Check for api method (if __toString is found it returns itself)
        if (!isset($parameter[1]) && !in_array('__toString', get_class_methods($module))) {
            throw new NotFoundException('No api method was given');
        }

        // Set module action
        $action = isset($parameter[1]) ? $parameter[1] : null;
        $action = $action . 'Action';

        // If couldn't be found
        if (!class_exists($module)) {
            throw new NotFoundException('Class for module ' . $module . ' not found');
        }

        // Check if method exists
        if (!method_exists($module, $action)) {

            // Ignore error if __toString is implemented
            if (in_array('__toString', get_class_methods($module))) {
                die(new $module($parameter));
            }

            throw new NotFoundException('Action ' . $action . ' not found');

        }

        $parameter = array_slice($parameter, 2, 1);

        // Run that shit
        return (new $module($parameter))->$action();

    }

}