<?php

namespace Api\Module;

use Core\Api\AbstractApiModule;
use Core\Http\Client;
use Core\Utility\Config;

class Image extends AbstractApiModule
{

    /** @var string */
    protected $sourceId;

    /** @var string */
    protected static $imgurDetailsRoute = 'https://api.imgur.com/3/gallery/album/%s';

    protected $apiConfig = [];

    /**
     * Image constructor.
     * @param array $parameter
     */
    public function __construct(array $parameter) {
        $this->sourceId = $parameter[0];
        $this->apiConfig = Config::get('api');
    }

    /**
     * Imgur's additional pictures
     */
    public function imgurAction()
    {
        $data     = [];
        $response = Client::get(sprintf(self::$imgurDetailsRoute, $this->sourceId), [
            'Authorization: Client-ID ' . $this->apiConfig['imgur_client_id']
        ]);

        $result = json_decode($response);

        foreach ($result->data->images as $image) {
            $data[] = str_replace('http://', 'https://', $image->link);
        }

        $this->success($data);
    }

}