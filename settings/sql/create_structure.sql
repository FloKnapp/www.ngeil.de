CREATE TABLE `Item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(12) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `source_id` varchar(32) DEFAULT '0',
  `distributor` varchar(128) NOT NULL,
  `title` text NOT NULL,
  `media` text NOT NULL,
  `is_gallery` int(2) NOT NULL DEFAULT '0',
  `image_count` int(11) NOT NULL DEFAULT '0',
  `score` int(11) NOT NULL DEFAULT '0',
  `source_score` varchar(16) DEFAULT '0',
  `source_link` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `uniqueIdPerSource` (`source_id`,`distributor`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8

CREATE TABLE `Distributor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8