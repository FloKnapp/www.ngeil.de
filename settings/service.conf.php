<?php

use Application\Service;

return [
    'service' => [
        'viewHelper' => [
        ],
        'invokable' => [
        ],
        'factory' => [
            Service\ItemService::class => Service\Factory\ItemServiceFactory::class
        ]
    ]
];