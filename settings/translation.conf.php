<?php
return [

    'translation' => [
        'ger_DE' => [
            'file_not_found'                     => 'Aufgrund unerwarteter Umstände siehst Du diese Seite. '
                . 'Bitte entschuldige.',
            'field_must_not_be_empty'            => 'Dieses Feld darf nicht leer sein.',
            'field_must_contain_number'          => 'Dieses Feld muss eine Nummer beinhalten.',
            'field_must_contain_valid_email'     => 'Dieses Feld muss eine valide E-Mail-Adresse beinhalten.',
            'please_fill_in_login_data'          => 'Bitte die Benutzerdaten eingeben',

            'abuse_headline'                     => 'Missbrauch melden',
            'abuse_notice'                       => 'Bitte beachte, dass Du eine gültige E-Mail-Adresse angibst, um den '
                . 'Missbrauch des Missbrauch-Formulars vorzubeugen... Was für eine Ironie.',
            'abuse_form_url_placeholder'         => 'Die betreffende URL',
            'abuse_form_email_placeholder'       => 'Deine E-Mail-Adresse',
            'abuse_submit_btn'                   => 'Missbrauch melden',
            'abuse_confirmation_headline'        => 'Missbrauch gemeldet!',
            'abuse_confirmation_notice'          => 'Vielen Dank für die Meldung. Ich werde dies direkt prüfen und '
                . 'Dich auf dem Laufenden halten.',

            'contact_headline'                   => 'Kontakt',
            'contact_form_firstname_placeholder' => 'Dein Vorname',
            'contact_form_lastname_placeholder'  => 'Dein Nachname',
            'contact_form_email_placeholder'     => 'Deine E-Mail-Adresse',
            'contact_form_message_placeholder'   => 'Deine Nachricht',
            'contact_submit_btn'                 => 'Nachricht senden',
            'contact_confirmation_headline'      => 'Nachricht erfolgreich gesendet!',
            'contact_confirmation_notice'        => 'Vielen Dank für Deine Nachricht. Ich werde diese umgehend lesen und '
                . ' bei Bedarf direkt beantworten. In der Regel ist das innerhalb von 1-2 Stunden der Fall.',

            'contact_link'                       => 'Kontakt',
            'abuse_link'                         => 'Missbrauch melden',

            'link_to_homepage'                   => 'Hier geht´s zur Startseite',

            'additional_images_btn'              => 'Lade weitere %d Bilder',

            'source_field'                       => 'Quelle',

        ],
        'en_EN' => [
            'file_not_found'                     => 'Unfortunately i don\'t know what happened. That\'s why you see '
                . 'this site. Please excuse me.',
            'field_must_not_be_empty'            => 'This field must not be empty.',
            'field_must_contain_number'          => 'This field must contain at least one number.',
            'field_must_contain_valid_email'     => 'This field must contain a valid email address.',
            'please_fill_in_login_data'          => 'Please fill in your login data',

            'abuse_headline'                     => 'Report abuse',
            'abuse_notice'                       => 'Please be aware, that you have to enter a valid email address to '
                . 'prevent abusive usage of this form. What an irony...',
            'abuse_url_placeholder'              => 'Enter here the url which contains abusive content',
            'abuse_form_email_placeholder'       => 'Your email address',
            'abuse_submit_btn'                   => 'Report abuse',
            'abuse_confirmation_headline'        => 'Abuse report successfully send',
            'abuse_confirmation_notice'          => 'Thank you for your report. I will proof this case immediately '
                . 'and contact you if necessary.',

            'contact_headline'                   => 'Contact',
            'contact_form_firstname_placeholder' => 'Your first name',
            'contact_form_lastname_placeholder'  => 'Your last name',
            'contact_form_email_placeholder'     => 'Your email address',
            'contact_form_message_placeholder'   => 'Your message',
            'contact_submit_btn'                 => 'Send message',
            'contact_confirmation_headline'      => 'Message successfully send',
            'contact_confirmation_notice'        => 'Thank you for your message. I will read it immediately and '
                . 'respond on it in need. Usually i respond within 1 and 2 hours',

            'contact_link'                       => 'Contact',
            'abuse_link'                         => 'Report abuse',

            'link_to_homepage'                   => 'Here it goes to the homepage',

            'additional_images_btn'              => 'Load %d more images',

            'source_field'                       => 'Source',
        ]
    ]

];