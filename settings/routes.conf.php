<?php

use Application\Controller;

return [

    'routes' => [

        'home' => [
            'route'  => '/',
            'ctrl'   => Controller\WebsiteController::class,
            'action' => 'index'
        ],

        'explore' => [
            'route'  => '/explore',
            'ctrl'   => Controller\WebsiteController::class,
            'action' => 'index',
            'static' => false
        ],

        'report_abuse' => [
            'route'  => '/report-abuse',
            'ctrl'   => Controller\WebsiteController::class,
            'action' => 'abuse',
            'static' => false
        ],

        'report_abuse_confirmation' => [
            'route'  => '/report-abuse-confirmation',
            'ctrl'   => Controller\WebsiteController::class,
            'action' => 'abuseConfirmation'
        ],

        'contact' => [
            'route'  => '/contact',
            'ctrl'   => Controller\WebsiteController::class,
            'action' => 'contact'
        ],

        'contact_confirmation' => [
            'route'  => '/contact-confirmation',
            'ctrl'   => Controller\WebsiteController::class,
            'action' => 'contactConfirmation'
        ],

        'api_image_additional' => [
            'route'  => '/api/image',
            'ctrl'   => \Api\ApiController::class,
            'static' => false
        ]

    ]

];