<?php

namespace Application\Form\Validator;

use Core\Form\AbstractValidator;

class ContactValidator extends AbstractValidator
{

    /**
     * @return array
     */
    public function validationOptions()
    {
        return [
            'email' => ['not_empty', 'is_email'],
            'message' => ['not_empty', 'is_string']
        ];
    }

}