<?php

namespace Application\Form\Validator;

use Core\Form\AbstractValidator;

class AbuseValidator extends AbstractValidator
{

    /**
     * @return array
     */
    public function validationOptions()
    {
        return [
            'uri' => ['not_empty', 'is_string'],
            'email' => ['not_empty', 'is_email']
        ];
    }

}