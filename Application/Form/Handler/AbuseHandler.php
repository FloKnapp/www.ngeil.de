<?php

namespace Application\Form\Handler;

use Application\Form\Validator\AbuseValidator;
use Core\Form\AbstractFormHandler;
use Core\Mail\Mailer;
use Core\Utility\Config;
use Core\Utility\Http;

class AbuseHandler extends AbstractFormHandler
{

    public function run()
    {
        $personalConfig = Config::get('personal');

        $validator = new AbuseValidator();
        $isValid = $this->validate($validator);

        if ($isValid !== true) {
            Http::redirect($this->errorUrl);
        }

        $data = $this->getFormData();

        $text = <<<TEXT
Hallo,<br /><br />
es wurde ein Missbrauchsverdacht geäußert. Die betreffende Url lautet:
<p>https://www.ngeil.de{$data['uri']}</p>
Gemeldet wurde der Verdacht von einem Nutzer mit der E-Mail-Adresse <b>{$data['email']}</b>.
TEXT;


        $mail = new Mailer();
        $mail->addTo($personalConfig['abuse_report_email']);
        $mail->setSubject('ngeil.de | Missbrauchsverdacht');
        $mail->setHtmlBody($text);

        if ($mail->send()) {
            Http::redirect($this->successUrl);
        }

        exit(0);
    }

}