<?php

namespace Application\Form\Handler;

use Application\Form\Validator\ContactValidator;
use Core\Form\AbstractFormHandler;
use Core\Mail\Mailer;
use Core\Utility\Config;
use Core\Utility\Http;

class ContactHandler extends AbstractFormHandler
{

    public function run()
    {
        $personalConfig = Config::get('personal');

        $validator = new ContactValidator();
        $isValid = $this->validate($validator);

        if ($isValid !== true) {
            Http::redirect($this->errorUrl);
        }

        $formData = $this->getFormData();

        $mail = new Mailer();
        $mail->addTo($personalConfig['contact_email']);
        $mail->setSubject('ngeil.de | Neue Nachricht über das Kontaktformular');

        $message = nl2br($formData['message']);

        $text = <<<TEXT
{$message}
<hr>
Absenderdaten:<br />
Vorname: {$formData['firstname']}<br />
Nachname: {$formData['lastname']}<br />
E-Mail: {$formData['email']}
TEXT;

        $mail->setHtmlBody($text);

        if ($mail->send()) {
            Http::redirect($this->successUrl);
        }

        Http::redirect($this->errorUrl);

        exit(0);
    }

}