<?php

namespace Application\Model\Entity;

use Core\Model\Entity\AbstractEntity;

/**
 * Class Item
 * @package Application\Model\Entity
 */
class Item extends AbstractEntity
{

    public static $tableName = 'Item';

    /** @var int */
    protected $id = 0;

    /** @var string */
    protected $uri = '';

    /** @var string */
    protected $created = NULL;

    /** @var string */
    protected $source_id = '';

    /** @var string */
    protected $distributor = '';

    /** @var string */
    protected $title = '';

    /** @var string */
    protected $media = '';

    /** @var string */
    protected $metadata = '';

    /** @var int */
    protected $is_gallery = 0;

    /** @var int */
    protected $image_count = 0;

    /** @var int */
    protected $score = 0;

    /** @var string */
    protected $source_score = '';

    /** @var string */
    protected $source_link = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getSourceId(): string
    {
        return $this->source_id;
    }

    /**
     * @param string $source_id
     */
    public function setSourceId(string $source_id)
    {
        $this->source_id = $source_id;
    }

    /**
     * @return string
     */
    public function getDistributor(): string
    {
        return $this->distributor;
    }

    /**
     * @param string $distributor
     */
    public function setDistributor(string $distributor)
    {
        $this->distributor = $distributor;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getMedia(): string
    {
        return $this->media;
    }

    /**
     * @param string $media
     */
    public function setMedia(string $media)
    {
        $this->media = $media;
    }

    /**
     * @return string
     */
    public function getMetadata(): string
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     */
    public function setMetadata(string $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return int
     */
    public function getIsGallery(): int
    {
        return $this->is_gallery;
    }

    /**
     * @param int $is_gallery
     */
    public function setIsGallery(int $is_gallery)
    {
        $this->is_gallery = $is_gallery;
    }

    /**
     * @return int
     */
    public function getImageCount(): int
    {
        return $this->image_count;
    }

    /**
     * @param int $image_count
     */
    public function setImageCount(int $image_count)
    {
        $this->image_count = $image_count;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score)
    {
        $this->score = $score;
    }

    /**
     * @return string
     */
    public function getSourceScore(): string
    {
        return $this->source_score;
    }

    /**
     * @param string $source_score
     */
    public function setSourceScore(string $source_score)
    {
        $this->source_score = $source_score;
    }

    /**
     * @return string
     */
    public function getSourceLink(): string
    {
        return $this->source_link;
    }

    /**
     * @param string $source_link
     */
    public function setSourceLink(string $source_link)
    {
        $this->source_link = $source_link;
    }

}