<?php

namespace Application\Model;

use Application\Model\Entity\Item;
use Core\Exceptions\NotFoundException;
use Core\Model\AbstractRepository;
use Core\Model\EntityMapper;
use Core\Model\EntityQueryBuilder;
use Core\Model\EntityRelationResolver;

/**
 * Class ItemRepository
 * @package Application\Model
 */
class ItemRepository extends AbstractRepository
{

    /**
     * @return array
     */
    public function getList()
    {
        $data     = [];
        $db       = $this->getDb();
        $entity   = $this->getEntity();
        $resolver = new EntityRelationResolver($entity);

        $query = new EntityQueryBuilder($resolver);
        $query->orderBy(['source_score' => 'DESC']);

        $raw = $db->query($query->getQuery());

        foreach ($raw AS $item) {
            $data[] = (new EntityMapper($resolver, $item))->mapData();
        }

        return $data;
    }

    /**
     * @return array|Item
     */
    public function getNewest()
    {
        $db       = $this->getDb();
        $entity   = $this->getEntity();
        $resolver = new EntityRelationResolver($entity);
        $query    = new EntityQueryBuilder($resolver);

        $query->orderBy(['id' => 'DESC']);
        $query->limit(1);

        $raw = $db->query($query->getQuery())->fetch_assoc();

        if (empty($raw)) {
            return [];
        }

        /** @var Item $data */
        $data = (new EntityMapper($resolver, $raw))->mapData();

        return $data;

    }

    /**
     * @param int $id
     * @return array|Item
     */
    public function getNextItem(int $id)
    {
        $db       = $this->getDb();
        $entity   = $this->getEntity();
        $resolver = new EntityRelationResolver($entity);
        $query    = new EntityQueryBuilder($resolver);
        $query    = $query->getQuery();

        $query .= ' WHERE id < ' . $id . ' ORDER BY id DESC LIMIT 1';

        $raw = $db->query($query)->fetch_assoc();

        if (empty($raw)) {
            return [];
        }

        /** @var Item $data */
        $data = (new EntityMapper($resolver, $raw))->mapData();

        return $data;
    }

    /**
     * @param int $id
     * @return array|Item
     */
    public function getPreviousItem(int $id)
    {
        $db       = $this->getDb();
        $entity   = $this->getEntity();
        $resolver = new EntityRelationResolver($entity);
        $query    = new EntityQueryBuilder($resolver);
        $query    = $query->getQuery();

        $query .= ' WHERE id > ' . $id . ' ORDER BY id ASC LIMIT 1';

        $raw = $db->query($query)->fetch_assoc();

        if (empty($raw)) {
            return [];
        }

        /** @var Item $data */
        $data = (new EntityMapper($resolver, $raw))->mapData();

        return $data;
    }

    /**
     * @param $uri
     * @return array|Item
     */
    public function getOneByUri(string $uri)
    {
        $db   = $this->getDb();
        $entity = $this->getEntity();
        $resolver = new EntityRelationResolver($entity);

        $query = new EntityQueryBuilder($resolver);
        $query->where(['uri' => $uri]);

        $data = $db->query($query->getQuery())->fetch_assoc();

        if (empty($data)) {
            return [];
        }

        /** @var Item $result */
        $result = (new EntityMapper($resolver, $data))->mapData();

        return $result;
    }

    public function getTotalCount()
    {
        $db = $this->getDb();
        $entity = $this->getEntity();

        $resolver = new EntityRelationResolver($entity);

        $query = new EntityQueryBuilder($resolver);
        $countQuery = $query->selectCount('id');

        $result = $db->query($countQuery)->fetch_row();

        return $result[0];

    }

}