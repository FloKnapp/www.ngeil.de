<?php

namespace Application\Controller;

use Application\Service\ItemService;
use Core\Controller\AbstractController;
use Core\Exceptions\NotFoundException;

/**
 * Class WebsiteController
 * @package Application\Controller
 */
class WebsiteController extends AbstractController
{

    /** @var string */
    public static $requiredPermission = '';

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * Render the main landing page
     *
     * @param array $parameter
     *
     * @return string
     * @throws NotFoundException If no data from database received
     */
    public function indexAction($parameter)
    {
        $this->addDefaultAssets();
        $this->addAsset('/js/keyboardNavigation.js');

        /** @var ItemService $itemService */
        $itemService  = $this->getServiceLocator()->get(ItemService::class);
        $totalCount   = $itemService->getItemCount();
        $previousItem = null;

        if ($parameter) {

            $item = $itemService->getItemByUri($parameter[0]);

            if (empty($item)) {
                throw new NotFoundException('No data found');
            }

            $previousItem = $itemService->getPreviousItem($item->getId());

        } else {

            $item = $itemService->getNewestItem();

        }

        if (empty($item)) {
            throw new NotFoundException('No data found');
        }

        $nextItem     = $itemService->getNextItem($item->getId());

        return $this->render('/content/index.phtml', ['item' => $item, 'nextItem' => $nextItem, 'previousItem' => $previousItem, 'totalCount' => $totalCount]);

    }

    /**
     * @return string
     */
    public function contactAction()
    {
        $this->addDefaultAssets();
        $this->addAsset('/css/form.css');
        return $this->render('/content/contact.phtml');
    }

    /**
     * @return string
     */
    public function contactConfirmationAction()
    {
        $this->addDefaultAssets();
        return $this->render('/content/contact-confirmation.phtml');
    }

    /**
     * @param array $parameter
     * @return string
     * @throws NotFoundException
     */
    public function abuseAction($parameter)
    {
        $this->addDefaultAssets();
        $this->addAsset('/css/form.css');

        /** @var ItemService $itemService */
        $itemService = $this->getServiceLocator()->get(ItemService::class);
        $item = $itemService->getItemByUri($parameter[0]);

        if (empty($item)) {
            throw new NotFoundException('No valid path identifier for abuse alert found');
        }

        return $this->render('/content/report-abuse.phtml', ['uri' => $parameter[0]]);
    }

    /**
     * @return string
     * @throws NotFoundException
     */
    public function abuseConfirmationAction()
    {
        $this->addDefaultAssets();
        return $this->render('/content/report-abuse-confirmation.phtml');
    }

    /**
     * Helper for adding default assets which are used in different methods
     */
    private function addDefaultAssets()
    {
        $this->addAsset('https://fonts.googleapis.com/css?family=Monda|Roboto+Condensed');
        $this->addAsset('/css/main.css');
        $this->addAsset('/css/font-awesome/font-awesome.min.css');
        $this->addAsset('/js/namespace.js');
        $this->addAsset('/js/common.js');
        $this->addAsset('/js/imageZoom.js');
        $this->addAsset('/js/imageAdditional.js');

    }

}