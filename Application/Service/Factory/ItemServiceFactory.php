<?php

namespace Application\Service\Factory;

use Application\Model\Entity\Item;
use Application\Model\ItemRepository;
use Application\Service\ItemService;
use Core\Service\FactoryInterface;
use Core\Service\ServiceLocatorInterface;

/**
 * Class NewestItemServiceFactory
 * @package Application\Service\Factory
 */
class ItemServiceFactory implements FactoryInterface
{

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ItemService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $ir = new ItemRepository(Item::class);
        return new ItemService($ir);
    }

}