<?php

namespace Application\Service;

use Application\Model\ItemRepository;

class ItemService
{

    /** @var array */
    protected $ir = null;

    /**
     * ItemService constructor.
     * @param $ir
     */
    public function __construct(ItemRepository $ir)
    {
        $this->ir = $ir;
    }

    public function getNewestItem()
    {
        return $this->ir->getNewest();
    }

    public function getPreviousItem($id)
    {
        return $this->ir->getPreviousItem($id);
    }

    public function getNextItem($id)
    {
        return $this->ir->getNextItem($id);
    }

    public function getItemByUri($uri)
    {
        return $this->ir->getOneByUri($uri);
    }

    public function getItemCount()
    {
        return $this->ir->getTotalCount();
    }

}