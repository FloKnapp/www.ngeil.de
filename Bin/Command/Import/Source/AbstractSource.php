<?php

namespace Bin\Command\Import\Source;

use Core\Dal\Database;
use Core\Dal\Driver\MySql;

abstract class AbstractSource
{

    /** @var string */
    protected $distributor = '';

    /**
     * AbstractSource constructor.
     */
    public function __construct()
    {
        print 'Executing source ' . str_replace(__NAMESPACE__, '', get_class($this)) . "\n";
    }

    /**
     * @return string
     */
    protected function generateRandomIdentifier()
    {
        /** @var Database $db */
        $db = new Database('mysql');

        $result = '';
        $chars  = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        for ($i = 0; $i < 8; $i++) {
            $result .= $chars{mt_rand(0,51)};
        }

        /** @var MySql $driver */
        $driver = $db->getDriver();

        /** @var \mysqli_result $exists */
        $exists = $driver->query('SELECT `uri` FROM `Item` WHERE `uri` = \'' . $result . '\'');

        if (mysqli_num_rows($exists) > 0) {
            return $this->generateRandomIdentifier();
        }

        return $result;
    }

    /**
     * Does the main magic
     */
    public function execute() {}

    /**
     * @param \stdClass $dataset
     * @return array
     */
    protected function getEntityData(\stdClass $dataset) {}

    /**
     * @param \stdClass $dataset
     * @return array
     */
    protected function getMediaFromDataset(\stdClass $dataset) {}

    /**
     * @param \stdClass $dataset
     * @return string
     */
    protected function getSourceLink(\stdClass $dataset) {}

    /**
     * @param \stdClass $dataset
     * @return array
     */
    protected function getMediaType($dataset) {}

}