<?php

namespace Bin\Command\Import\Source;

use Application\Model\Entity\Item;
use Application\Model\ItemRepository;
use Core\Exceptions\DatabaseDuplicateKeyException;
use Core\Http\Client;
use Core\Utility\Config;

class Imgur extends AbstractSource
{

    /** @inheritDoc */
    protected $distributor = 'Imgur';

    /** @var string */
    protected $url = 'https://api.imgur.com/3/topics/Funny/top';

    /** @var string */
    protected $albumDetailsUrl = 'https://api.imgur.com/3/gallery/album/%s';

    /** @var string */
    protected $imageDetailsUrl = 'https://api.imgur.com/3/image/%s';

    /** @var string */
    protected $sourceLinkPrefix = 'https://imgur.com/topic/Funny';

    /** @var int */
    protected $insertCounter = 0;

    /** @var int */
    protected $updateCounter = 0;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $clientId = Config::get('api')['imgur_client_id'];

        $response = Client::get(sprintf($this->url, 1), [
            'Authorization: Client-ID ' . $clientId
        ]);

        $result = json_decode($response);

        foreach ($result->data as $item) {

            $score = $item->score / $item->views * 100;

            if ($item->topic !== 'Funny' && ($score < 10 || $item->topic === 'Creativity')) {
                continue;
            }

            print 'Processing source_id ' . $item->id . '... ';

            $media = $this->getMediaFromDataset($item);

            if (empty($media)) {
                print "\n". 'no media found - skipped';
                continue;
            }

            $entity = new Item();
            $entity->fill($this->getEntityData($item));
            $entity->setCreated(NULL);

            $ir = new ItemRepository($entity);
            $ir->save();

            if ($ir->getStrategy() === 'update') {
                print 'updating... ';
                $this->updateCounter++;
            } else if ($ir->getStrategy() === 'insert') {
                $this->insertCounter++;
            }

            print 'done' . "\n";

        }

        print "\n" . $this->updateCounter . ' items successfully updated' . "\n";
        print $this->insertCounter . ' items successfully imported' . "\n\n";

    }

    /**
     * @param \stdClass $dataset
     * @return array
     */
    protected function getEntityData(\stdClass $dataset)
    {

        $mediaData = $this->getMediaFromDataset($dataset);
        $media = $mediaData['media'];
        unset($mediaData['media']);

        $sourceLink = $this->getSourceLink($dataset);

        return [
            'uri'          => $this->generateRandomIdentifier(),
            'source_id'    => $dataset->id,
            'distributor'  => $this->distributor,
            'title'        => htmlentities($dataset->title),
            'media'        => $media,
            'metadata'     => json_encode($mediaData),
            'is_gallery'   => isset($dataset->images_count) && $dataset->images_count > 1 ? 1 : 0,
            'image_count'  => isset($dataset->images_count) ? $dataset->images_count : 1,
            'score'        => $score = $dataset->score / $dataset->views * 100,
            'source_score' => $dataset->score,
            'source_link'  => htmlentities($sourceLink)
        ];
    }

    /**
     * @param \stdClass $dataset
     * @return array
     */
    protected function getMediaFromDataset(\stdClass $dataset)
    {

        $media    = [];
        $clientId = Config::get('api')['imgur_client_id'];

        if ($dataset->is_album === true) {

            $response = Client::get(sprintf($this->imageDetailsUrl, $dataset->cover), [
                'Authorization: Client-ID ' . $clientId
            ]);

            /** @var \stdClass $image */
            $image = json_decode($response);
            $image = $image->data;

        } else {

            $image = $dataset;

        }

        $typeInfo = $this->getMediaType($image->type);

        if ($typeInfo['type'] === 'video') {

            if ($image->animated === true) {

                $media['media']    = str_replace('http://', 'https://' , $image->mp4);
                $media['mimetype'] = 'video/mp4';

            } else {

                $media['media']    = str_replace('http://', 'https://' , $image->link);
                $media['mimetype'] = 'image/gif';

            }

        }

        if ($typeInfo['type'] === 'image') {

            $media['media']    = str_replace('http://', 'https://' , $image->link);
            $media['mimetype'] = $image->type;

        }

        $media['width']    = $image->width;
        $media['height']   = $image->height;
        $media['size']     = $image->size;

        return $media;
    }

    /**
     * @inheritdoc
     */
    protected function getMediaType($mimeType)
    {
        $result = [];

        switch ($mimeType) {

            case 'image/jpeg':
            case 'image/jpg':
                $result['type'] = 'image';
                $result['suffix'] = 'jpg';
                break;

            case 'image/png':
                $result['type'] = 'image';
                $result['suffix'] = 'png';
                break;

            case 'image/gif':
            case 'video/mp4':
            $result['type'] = 'video';
            $result['suffix'] = 'mp4';
                break;

        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function getSourceLink(\stdClass $dataset)
    {
        return $this->sourceLinkPrefix . '/' . $dataset->id;
    }

}