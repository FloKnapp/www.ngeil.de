<?php
/**
 * Created by PhpStorm.
 * User: Flo
 * Date: 15.11.2016
 * Time: 21:15
 */

namespace Bin\Command\Import\Source;


use Application\Model\Entity\Item;
use Application\Model\ItemRepository;
use Core\Exceptions\DatabaseDuplicateKeyException;
use Core\Http\Client;
use Core\Utility\Config;

class Pinterest extends AbstractSource
{

    /** @inheritDoc */
    protected $distributor = 'Pinterest';

    protected $urlPrefix = 'https://api.pinterest.com/v1';

    protected $urlSuffix = '/?access_token=%s&fields=id,image,link,media,note,original_link,url';

    /** @var string */
    //protected $boardUrl = '/boards/NiagaraNovalis/funny-humor-deutsch/pins';
    protected $boardUrl = '/boards/leonardo_longo/humor-deutsch/pins';

    /** @var string */
    protected $imageDetailsUrl = '/pins/%s';

    /** @var int */
    protected $insertCounter = 0;

    /** @var int */
    protected $updateCounter = 0;

    /**
     * @inheritdoc
     */
    public function execute()
    {

        $accessToken = Config::get('api')['pinterest_access_token'];

        $getAll = true;

        $dataset = [];

        $uri = $this->urlPrefix . $this->boardUrl . sprintf($this->urlSuffix, $accessToken);

        while ($getAll === true) {

            $response = Client::get($uri);
            $result = json_decode($response);
            $data = $result->data;

            foreach ($data as $dataitem) {
                $dataset[] = $dataitem;
            }

            if ($result->page->next === null) {
                $getAll = false;
            } else {
                $uri = $result->page->next;
            }

        }

        foreach ($dataset as $item) {

            print 'Processing source_id ' . $item->id . '... ';

            $media = $this->getMediaFromDataset($item);

            if (empty($media)) {
                print "\n". 'no media found - skipped';
                continue;
            }

            $entity = new Item();
            $entity->fill($this->getEntityData($item));

            $ir = new ItemRepository($entity);
            $ir->save();

            if ($ir->getStrategy() === 'update') {
                print 'updating... ';
                $this->updateCounter++;
            } else if ($ir->getStrategy() === 'insert') {
                $this->insertCounter++;
            }

            print 'done' . "\n";

        }

    }

    protected function getEntityData(\stdClass $dataset)
    {

        $mediaData = $this->getMediaFromDataset($dataset);
        $media = $mediaData['media'];
        unset($mediaData['media']);

        $sourceLink = $this->getSourceLink($dataset);

        $title = empty(trim($dataset->note)) ? 'Ohne Titel' : htmlentities($dataset->note);

        return [
            'uri'          => $this->generateRandomIdentifier(),
            'source_id'    => $dataset->id,
            'distributor'  => $this->distributor,
            'title'        => $title,
            'media'        => $media,
            'metadata'     => json_encode($mediaData),
            'is_gallery'   => 0,
            'image_count'  => 1,
            'score'        => 0,
            'source_score' => 0,
            'source_link'  => htmlentities($sourceLink)
        ];
    }

    /**
     * @param \stdClass $dataset
     * @return array|boolean
     */
    protected function getMediaFromDataset(\stdClass $dataset)
    {
        $media = [];
        $type  = $dataset->media->type;

        if ($type !== 'image') {
            return false;
        }

        $media['media']    = str_replace('http://', 'https://' , $dataset->image->original->url);
        $media['mimetype'] = 'image/jpg';
        $media['width']    = $dataset->image->original->width;
        $media['height']   = $dataset->image->original->height;
        $media['size']     = 0;

        return $media;
    }

    /**
     * @inheritdoc
     */
    protected function getSourceLink(\stdClass $dataset)
    {
        return $dataset->url;
    }
}