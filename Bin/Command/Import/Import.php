<?php

namespace Bin\Command\Import;

use Bin\Command\Import\Source\Imgur;
use Bin\Command\Import\Source\Pinterest;
use Core\Log\Logger;

class Import
{

    public function execute()
    {
        $log = new Logger();
        $log->write(date("\n" . 'd.m.Y H:i:s', time()) . ' | Import triggered', 'debug');

        $imgur = new Imgur();
        $imgur->execute();

        $pinterest = new Pinterest();
        $pinterest->execute();

        $log->write(date("\n" . 'd.m.Y H:i:s', time()) . ' | Import done', 'debug');
    }

}